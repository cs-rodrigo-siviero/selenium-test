package tests;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.File;
//Augmented Screenshot
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.Augmenter;

public class SeleniumChromeTest {
	@Test
   public void myTest() throws Exception {
       WebDriver driver = new RemoteWebDriver(
                               new URL("http://localhost:4444/wd/hub"),
                               DesiredCapabilities.chrome());
       System.out.println("Browser: Chrome");
       System.out.println("Opening Website");
       driver.get("http://www.google.com");
       System.out.println("Testing Title");
			 WebDriver augmentedDriver = new Augmenter().augment(driver);
			 File screenshot = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
			 FileUtils.copyFile(screenshot, new File("screenshot.png"));
       assertEquals("Google" , driver.getTitle());
       System.out.println("Expected is: Google");
       System.out.println("Title is: " + driver.getTitle());
       driver.close();
       driver.quit();
       System.out.println("Browser Closed");
   }
	// @Test
	// public void helloWorld() throws Exception {
  //       WebDriver driver = new RemoteWebDriver(
  //               new URL("http://seleniuminf.duckdns.org:4444/wd/hub"),
  //               DesiredCapabilities.chrome());
  //       System.out.println("Browser: Chrome");
  //       System.out.println("Opening Website");
  //       driver.get("http://seleniumpoc.duckdns.org/selenium/");
  //       WebElement divElement = driver.findElement(By.className("helloworld"));
	// 	String str = divElement.getText();
	// 	assertEquals("Hello World!" , str);
	// 	System.out.println(str);
	// 	driver.close();
  //       driver.quit();
  //       System.out.println("Browser Closed");
	// }
}
